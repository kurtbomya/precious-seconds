# Use the official nginx image as the parent image
FROM nginx

# Copy the index.html file to the container
RUN mkdir -p /usr/share/nginx/html/count
COPY index.html /usr/share/nginx/html/count
