# Precious Seconds

Life goes too fast, keep a reminder up of how little time we get with our children before they move along. Invest it well.

### Deployment
- `nginx` container
- presenting `bootstrap5` single page web application
- staged onto GitLab.com